import React, { Component } from 'react'
import './index.css'

class Square extends Component {
  render () {
    let { id, isActive, handleSquareClick } = this.props
    let className = 'Square ' + isActive[id]

    return (
      <button onClick={() => handleSquareClick(id)} className={className} />
    )
  }
}

export default Square
