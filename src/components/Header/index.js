import React, { Component } from 'react'
import logo from '../../logo.png'
import './index.css'

class Header extends Component {
  render () {
    return (
      <div className='Header'>
        <img src={logo} className='Header-logo' alt='logo' />
      </div>
    )
  }
}

export default Header
