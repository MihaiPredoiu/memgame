import React, { Component } from 'react'
import Square from '../Square'
import Timer from '../Timer'
import data from './data.json'
import './index.css'

class Board extends Component {
  constructor (props) {
    super(props)

    this.state = {
      suite: 0,
      test: -1,
      timer: 2,
      clicksLeft: data[0].tests[0].length,
      isWinning: data[0].tests[0],
      isActive: new Array(100).fill('')
    }

    this.handleSquareClick = this.handleSquareClick.bind(this)
    this.handleCheckClick = this.handleCheckClick.bind(this)
    this.tick = this.tick.bind(this)
    this.startGame = this.startGame.bind(this)
  }

  loadNewGame () {
    let { onGameEnd } = this.props
    let { suite, test } = this.state

    if (suite === data.length - 1 && test === data[suite].tests.length - 1) {
      onGameEnd()
      return
    }

    if (test < data[suite].tests.length - 1) {
      test++
    } else {
      suite++
      test = 0
    }

    let isWinningCopy = data[suite].tests[test]
    let isActiveCopy = new Array(100).fill('')

    this.setState({
      ...this.state,
      isActive: isActiveCopy
    })

    for (let i = 0; i < isWinningCopy.length; i++) {
      isActiveCopy[isWinningCopy[i]] = 'blue'
    }

    this.setState({
      ...this.state,
      suite: suite,
      test: test,
      timer: 2,
      isWinning: isWinningCopy,
      isActive: isActiveCopy
    })
  }

  startGame () {
    let { suite, test } = this.state
    let activeCells = data[suite].tests[test]
    let isActiveCopy = this.state.isActive.slice()

    for (let i = 0; i < activeCells.length; i++) {
      isActiveCopy[activeCells[i]] = ''
    }

    this.setState({ ...this.state, isActive: isActiveCopy })
    clearInterval(this.timer)
  }

  tick () {
    this.setState({ ...this.state, timer: this.state.timer - 1 })

    if (this.state.timer === 0) {
      this.startGame()
    }
  }

  handleCheckClick () {
    const { updateScore } = this.props
    let { isActive } = this.state

    let levelScore = isActive.reduce((sum, x) => {
      if (x === 'active') {
        return sum + 1
      }
      if (x === 'wrong') {
        return sum - 1
      }
      return sum
    }, 0)

    updateScore(levelScore)
    this.loadNewGame()
    this.timer = setInterval(this.tick, 1000)
  }

  handleSquareClick (id) {
    let { timer, isWinning, isActive } = this.state
    let isActiveCopy = isActive.slice()

    if (timer === 0) {
      if (isWinning.indexOf(id) >= 0) {
        isActiveCopy[id] = 'active'
      } else {
        isActiveCopy[id] = 'wrong'
      }

      if (isActive[id] === '') {
        this.setState({
          ...this.state,
          clicksLeft: this.state.clicksLeft - 1,
          isActive: isActiveCopy
        })
      }
    }
  }

  componentDidMount () {
    this.loadNewGame()
    this.timer = setInterval(this.tick, 1000)
  }

  componentWillUnmount () {
    clearInterval(this.timer)
  }

  get board () {
    let { suite, isActive } = this.state
    let { rows, columns } = data[suite]
    let cells = []
    let board = []
    let i
    let j

    // Create each cell of the board
    for (i = 0; i < rows; i++) {
      cells[i] = []
      for (j = 0; j < columns; j++) {
        cells[i][j] = (
          <Square
            key={rows * i + j}
            id={rows * i + j}
            isActive={isActive}
            handleSquareClick={this.handleSquareClick}
          />
        )
      }
      board[i] = <li key={i}>{cells[i]}</li>
    }

    return board
  }

  render () {
    let { timer } = this.state

    return (
      <div>
        <p className='board-score'>Score: {this.props.score}</p>
        <Timer left={timer} />
        <ul className='Board'>{this.board}</ul>
        <button onClick={this.handleCheckClick} className='Button'>
          Check
        </button>
      </div>
    )
  }
}

export default Board
