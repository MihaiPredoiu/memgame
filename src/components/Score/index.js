import React, { Component } from 'react'
import './index.css'

class Score extends Component {
  componentDidMount () {
    let { onExpiredInterval } = this.props

    setTimeout(onExpiredInterval, 5000)
  }

  render () {
    const { score } = this.props

    return (
      <div className='score'>
        <p className='score-text'>Your final score: {score}</p>
      </div>
    )
  }
}

export default Score
