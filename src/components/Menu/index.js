import React, { Component } from 'react'
import './index.css'

class Menu extends Component {
  render () {
    let { handleStartClick } = this.props

    return (
      <div className='Menu'>
        <div className='Description'>
          <p>MemGame has been made for testing the topographical memory.</p>
          <p>
            When pressing the START button, a matrix of squares will appear on
            the screen and some of them will be colored in blue. After two
            seconds of memorization, you will have to press those squares, then
            press the CHECK button.
          </p>
          <p>The correct ones will turn green and bring you one point.</p>
          <p>The wrong ones will turn red and bring you minus one point.</p>
        </div>
        <button onClick={handleStartClick} className='Button'>
          Start
        </button>
      </div>
    )
  }
}

export default Menu
