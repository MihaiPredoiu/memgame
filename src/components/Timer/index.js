import React, { Component } from 'react'
import './index.css'

class Timer extends Component {
  get paragraph () {
    const { left } = this.props

    if (left > 0) {
      return <p>Remaining time: {left}</p>
    }
    return (
      <p>
        <br />
      </p>
    )
  }

  render () {
    return <div className='Timer'>{this.paragraph}</div>
  }
}

export default Timer
