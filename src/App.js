import React, { Component } from 'react'
import Header from './components/Header'
import Board from './components/Board'
import Menu from './components/Menu'
import Score from './components/Score'
import './App.css'

import backArrow from './assets/backArrow.svg'

class App extends Component {
  state = {
    page: 'menu',
    score: 0
  }

  updateScore = newScore => {
    const { score } = this.state

    this.setState({ ...this.state, score: score + newScore })
  }

  handleStartClick = () => {
    this.setState({ ...this.state, page: 'board', score: 0 })
  }

  handleBackClick = () => {
    this.setState({ ...this.state, page: 'menu' })
  }

  handleGameEnd = () => {
    this.setState({ ...this.state, page: 'result' })
  }

  backButton = (
    <img
      className='backButton'
      onClick={this.handleBackClick}
      src={backArrow}
      alt='logo'
    />
  )

  render () {
    let { page, score } = this.state
    let content
    switch (page) {
      case 'menu':
        content = <Menu handleStartClick={this.handleStartClick} />
        break
      case 'board':
        content = (
          <div>
            <Board
              onBackClick={this.handleBackClick}
              onGameEnd={this.handleGameEnd}
              score={score}
              updateScore={this.updateScore}
            />
            {this.backButton}
          </div>
        )
        break
      case 'result':
        content = (
          <div>
            <Score score={score} onExpiredInterval={this.handleBackClick} />
            {this.backButton}
          </div>
        )
        break
      default:
        break
    }

    return (
      <div className='App'>
        <Header />
        {content}
      </div>
    )
  }
}

export default App
