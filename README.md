# Intro
MemGame is a simple game designed to test the topographic memory. It is available for demo at http://mem-games.herokuapp.com/.

# Screenshots with different game stages
![Screenshot](https://bitbucket.org/MihaiPredoiu/memgame/raw/master/previews/ss1.png "Menu")
![Screenshot](https://bitbucket.org/MihaiPredoiu/memgame/raw/master/previews/ss2.png "Gameplay in memorization stage")
![Screenshot](https://bitbucket.org/MihaiPredoiu/memgame/raw/master/previews/ss3.png "Gameplay in the stage of choosing correct squares")